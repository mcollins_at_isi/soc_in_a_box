#!/usr/bin/env python3
#
#
# igraph.py
#
# This is a basic inference graph command line interface for the SIAB
# system.  Since we're still in the prototype phase, i'm using
# sqlite which is compact and readily available.  We can scale up the
# speed using a memory implementation, but the next logical step is
# to implement this thing in a real graph database.
#
import argparse
import os
import sqlite3
import sys

actions = set(['count','add','init'])

types = {'as': str,
         'asn': int,
         'fqdn': str,
         'sip': str,
         'dip': str,
         'filename': str,
         'md5hash': str,
         'sport':int,
         'dport':int,
         'proto':int,
         'rule': str,
         'url': str,
         'misc': str}
          
         
parser = argparse.ArgumentParser(description = 'Manage inference graph')
parser.add_argument('action', help='Action taken by the utility')
parser.add_argument('--dbu', default='igraph.db', help='Database interface, may be a file or a url')
parser.add_argument('--link',default = '', help ='Add a link, specified as stype:src:dtype:dst')

# Basic database functinality here
#
#
def get_dbh(dbu):
    """
    get_dbh (dbu):
    Gets the database handle.  
    """
    con = sqlite3.connect(dbu)
    return con

# Utilities for creating the database
def init_db(con):
    """
    Creates the database from scratch if it doesn't exist
    """
    csr = con.cursor()
    try:
        csr.execute('DROP TABLE links')
    except:
        pass
    con.commit()
    csr.execute('CREATE TABLE links (stype txt, dtype txt, src txt, dst txt)')
    con.commit()


def verify_link(lstr):
    try:
        stype, sval, dtype, dval = lstr.split(':')
    except:
        sys.stderr.write('Error parsing link string "%s", aborting\n' % lstr)
        sys.stderr.flush()
        return None
    for t in (stype, dtype):
        if not t in types:
            sys.stderr.write('unrecognized type "%s", aborting\n' % t)
            sys.stderr.flush()
            return None
    # At this point, it's verified, return
    return(stype, sval, dtype, dval)

def add_link(con, stype, sval, dtype, dval):
    csr = con.cursor()
    csr.execute("INSERT INTO links VALUES('%s','%s','%s','%s')" % (stype, dtype, sval, dval))
    con.commit()

def count(con):
    """ 
    Counts a database
    """
    csr = con.cursor()
    a = csr.execute('SELECT COUNT(*) from links')
    result = a.fetchone()[0]
    return result
    
if __name__ == '__main__':
    args = parser.parse_args()
    dbh = get_dbh(args.dbu)
    if args.action in actions:
        if args.action == 'init':
            init_db(args.dbh)
        elif args.action == 'count':
            print(count(dbh))
        elif args.action == 'add':
            result = verify_link(args.link)
            if result is not None:
                add_link(dbh, result[0], result[1], result[2], result[3])

