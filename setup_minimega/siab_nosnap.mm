#
#
# This is a minimega configuration file specifically for the
# Minimega implementation of SOC-In-A-Box (SIAB)
#
# This implementation is used with scratch implementations;
# in practice you should configure each element with
# snapshot set to true
# Implementation works as follows
# The testbed implements two networks: CONTROL and ACTION
# Baseline images are configured to work with both networks
# CONTROL has a dnsmasq server set to 10.0.0.1/24
# ACTION addresses are statically allocated
#
# The system has the following assets:
# elk		10.0.2.225
# redmine	10.0.2.226
# misp		10.0.2.227
# glpi		10.0.2.228
# igraph	10.0.2.229
# sensor00	10.0.2.192
# sensor01	10.0.2.193
vm config net CONTROL ACTION
tap create CONTROL ip 10.0.0.1/24
dnsmasq start 10.0.0.1 10.0.0.2 10.0.0.254

vm config disk /home/mcollins/Desktop/siab_mm/elk.qc2
vm config snapshot false
vm config memory 16392
vm config vcpus 4
vm launch kvm elk

vm config disk /home/mcollins/Desktop/siab_mm/redmine.qc2
vm launch kvm redmine

vm config disk /home/mcollins/Desktop/siab_mm/misp.qc2
vm launch kvm misp

vm config disk /home/mcollins/Desktop/siab_mm/glpi.qc2
vm launch kvm glpi


vm config memory 8192
vm config vcpus 2
vm config disk /home/mcollins/Desktop/siab_mm/igraph.qc2
vm launch kvm igraph

vm config disk /home/mcollins/Desktop/siab_mm/sensor00.qc2
vm launch kvm sensor00

vm config disk /home/mcollins/Desktop/siab_mm/sensor01.qc2
vm launch kvm sensor01

vm start elk
vm start redmine
vm start misp
vm start glpi
vm start igraph
vm start sensor00
vm start sensor01



