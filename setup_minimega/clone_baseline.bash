#!/usr/bin/env bash
#
# This sets up all the various sensors from a baseline
# image.  It's really just memorizing all the various machines
# we need.
#
# We take two optional arguments
# ${1} is the name of the baseline file
# ${2} is the number of sensors

default_fn='baseline.qc2'
if [ -z ${1} ]
then
    default_fn='baseline.qc2'
else
    default_fn=${1}
fi

default_count=2
if [ -z ${2} ]
then
    default_count=2
else
    default_count=${2}
fi

for i in elk redmine misp glpi igraph
do
    cp ${default_fn} ${i}.qc2
done
i=0
while [ ${i} -lt ${default_count} ]
do

    cp ${default_fn} sensor`printf %02d ${i}`.qc2
    i=$[ ${i} + 1 ]
done


