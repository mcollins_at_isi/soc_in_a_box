# Minimega Installation
This directory contains configurations and tools for minimega installation.

## Inventory

## HOWTO

There are two baseline installation files here -- siab_nosnap.mm and siab_snap.mm.  

siab_nosnap.mm is designed to create a fresh installation from scratch with persistent images.  This is done with a baseline image (baseline.qc by default), which is then copied to each constituent component and set up without using snapshots. 

siab_snap.mm assumes that you have preconfigured images set up as snapshots. 

