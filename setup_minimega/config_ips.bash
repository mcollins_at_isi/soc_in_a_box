#!/usr/bin/env bash
#
# Configs the external IP addresses for the named systems.
names=(elk redmine misp glpi igraph)
ips=(10.0.2.225 10.0.2.226 10.0.2.227 10.0.2.228 10.0.2.229)
sensor_baseline=192
sensor_count=2

if [ -z ${1} ]
then
    sensor_count=2
else
    sensor_count=${1}
fi


set_interface() {
    ssh root@$1 "ip addr add dev ens2 ${2}/24 ; ip link set dev ens2 up"
}

index=0
for i in ${!names[@]}
do
    ip=`bash ./getnames.bash | grep ${names[$i]} | cut -d ' ' -f 3`
    echo "${names[$i]} ${ip} ${ips[$i]}" 
    set_interface ${ip} ${ips[$i]}
done

i=0
while [ ${i} -lt ${sensor_count} ]
do
    sname="sensor`printf %02d ${i}`"
    new_ip="10.0.2.$[ $i + ${sensor_baseline} ]"
    ip=`bash ./getnames.bash | grep ${sname} | cut -d ' ' -f 3`
    echo "${sname} ${ip} ${new_ip}"
    set_interface ${ip} ${new_ip}
    i=$[ ${i} + 1 ]
done

