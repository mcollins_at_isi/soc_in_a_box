#!/usr/bin/env bash
#
# Runs as sudo
# Creates an ini format hosts file
for i in `sudo minimega -e ".columns name,ip vm info " | grep '\[' | cut -d ',' -f 1 | cut -d '|' -f 2,3 | tr -d '|[' | tr -s ' ' | tr ' ' ':' | cut -b 2-`
do
    name=`echo ${i} | cut -d ':' -f 1`
    ip=`echo ${i} | cut -d ':' -f 2`
    echo "[$name]"
    echo ${ip}
done
