#!/usr/bin/env bash
#
# Runs as sudo
# gets the names and ips for vms in the system
sudo minimega -e ".columns name,ip vm info " | grep '\[' | cut -d ',' -f 1 | cut -d '|' -f 2,3 | tr -d '|[' | tr -s ' '
