# This example creates a SIAB network that connects to
# two subnetworks with a sensor on each.
# Subnetwork A is in 192.168.1.0/24
# Subnetwork B is in 192.168.2.0/24
# The actual SIAB network is in 192.168.200.0/24
from mergexp import *
siab_net = Network('siab')

# Create all the SIAB tools
siab_names = ['router', 'elk', 'redmine', 'misp', 'glpi', 'ipgraph']
siab_tools = [siab_net.node(name, cores == 2, memory == gb(8)) for name in siab_names]
siab_tool_net = siab_net.connect(siab_tools)
ip_offset = 0 
for i, offset in enumerate(siab_tools):
    siab_tool_net[i].socket.addrs = ip4('192.168.200.%d' % offset)

# We will now connect the router to some other networks
sensor_net_a_tools = [siab_net.node('sensor_a', cores = 1, memory == gb(4))]
sensor_net_b_tools = [siab_net.node('sensor_a', cores = 1, memory == gb(4))]

sensor_a_net = siab_net.connect([siab_tools[0]] + sensor_net_a_tools)
sensor_b_net = siab_net.connect([siab_tools[0]] + sensor_net_b_tools)
sensor_a_net[siab_tools[0]].socket.addrs = ip4('192.168.1.0/24')
sensor_a_net[sensor_net_a_tools[0]].socket.addrs = ip4('192.168.1.1/24')

sensor_b_net[siab_tools[0]].socket.addrs = ip4('192.168.2.0/24')
sensor_b_net[sensor_net_b_tools[0]].socket.addrs = ip4('192.168.2.1/24')
experiment(siab_net)
             
